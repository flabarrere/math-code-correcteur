package scrambler;

import bitsequence.BitSequence;

public interface Scrambler {
    BitSequence scramble(BitSequence bitSequence);
}
