package scrambler;

import bitsequence.BitSequence;

public class RandomScrambler implements Scrambler {

    double probError;

    public RandomScrambler( double probError){
        this.probError = probError;
    }

    @Override
    public BitSequence scramble(BitSequence bitSequence) {

        boolean oldSequence[] = bitSequence.toArray();
        boolean newSequence[] = new boolean[bitSequence.length()];
        for (int i=0 ; i<bitSequence.length() ; i++ )
            newSequence[i] = oldSequence[i] ^ (Math.random()<probError);

        return new BitSequence(newSequence);
    }
}
