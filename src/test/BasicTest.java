package test;

import bitsequence.BitSequence;
import codecorrecteur.CodeCorrecteur;
import codecorrecteur.ParityCodeCorrecteur;

public class BasicTest {
    public static void test(CodeCorrecteur codeCorrecteur, String message ){

        BitSequence bitSequenceOriginal = new BitSequence(message);
        BitSequence bitSequenceSent;
        BitSequence bitSequenceDecode;
        boolean allRight;

        bitSequenceSent = codeCorrecteur.encoder( bitSequenceOriginal );
        bitSequenceDecode = codeCorrecteur.decoder( bitSequenceSent );
        allRight = codeCorrecteur.verify( bitSequenceSent );

        System.out.println( "Test du " + codeCorrecteur.getName()+" avec le message "+ bitSequenceOriginal);
        System.out.println( "Envoie de "+ bitSequenceSent);
        System.out.println( "Decodage en "+ bitSequenceDecode);
        System.out.println( allRight? "Tout va bien" : "Erreur");

    }

    public static void main(String[] argv){
        test(new ParityCodeCorrecteur(), "0101011");
    }
}
