package test;

import bitsequence.BitSequence;
import codecorrecteur.CodeCorrecteur;
import codecorrecteur.HammingCodeCorrecteur;
import codecorrecteur.ParityCodeCorrecteur;
import scrambler.RandomScrambler;
import scrambler.Scrambler;

public class PerturbationTest {
    public static void test(CodeCorrecteur codeCorrecteur, String message, Scrambler scrambler ){

        BitSequence bitSequenceOriginal = new BitSequence(message);
        BitSequence bitSequenceSent;
        BitSequence bitSequenceReceive;
        BitSequence bitSequenceDecode;
        boolean allRight;

        bitSequenceSent = codeCorrecteur.encoder( bitSequenceOriginal );
        bitSequenceReceive = scrambler.scramble(bitSequenceSent);
        bitSequenceDecode = codeCorrecteur.decoder( bitSequenceReceive );
        allRight = codeCorrecteur.verify( bitSequenceReceive );

        System.out.println( "Test du " + codeCorrecteur.getName()+" avec le message "+ bitSequenceOriginal);
        System.out.println( "Envoie de "+ bitSequenceSent);
        System.out.println( "Reception de "+ bitSequenceReceive);
        System.out.println( "Decodage en "+ bitSequenceDecode);
        System.out.println( allRight? "Tout va bien" : "Erreur");

    }

    public static void main(String[] argv){
        test(new HammingCodeCorrecteur(), "0101011", new RandomScrambler(0.01));
    }
}
