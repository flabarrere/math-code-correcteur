package codecorrecteur;

import bitsequence.BitSequence;
import bitsequence.BitMatrix;

import java.util.Iterator;

public class CLSCodeCorrecteur extends CodeCorrecteur {

    private static final int blockSize = 6;
    private static final int defaultKeyLineCount = 2;

    private int keyLineCount ;

    public CLSCodeCorrecteur(){
        CodeCorrecteur.name = "Codage lineaire systematyque";
        CodeCorrecteur.corrector = false;
        keyLineCount = defaultKeyLineCount;
    }




    @Override
    public BitSequence encoder(BitSequence inputBitSequence, boolean verbose) {

        BitMatrix generativeMatrix = new BitMatrix( blockSize, blockSize+keyLineCount );
        generativeMatrix.add( BitMatrix.getIdendityMatrix( blockSize));
        for( int j=0 ; j<keyLineCount ; j++ )
            for( int i=0 ; i<blockSize ; i++ )
                generativeMatrix.set( i, blockSize+j,  (int)(i/Math.pow( 2, (double)j ) )%2==0 );

        if( verbose ){
            System.out.println("Matrice generatrice :");
            System.out.println( generativeMatrix );
        }

        Iterator<Boolean> input = inputBitSequence.iterator();
        BitSequence output = new BitSequence();
        BitMatrix inputMatrix = new BitMatrix( 1, blockSize );

        while ( input.hasNext() ){
            for( int i=0 ; i<blockSize ; i++)
                inputMatrix.set( 0, i, input.hasNext()? input.next() : false );
            output.append( BitMatrix.multiply( generativeMatrix, inputMatrix ).getColumn(0) );
        }


        return output ;
    }

    @Override
    public BitSequence decoder(BitSequence inputBitSequence, boolean verbose) {

        BitSequence output = new BitSequence();
        boolean[] buffer = new boolean[blockSize];

        if( verbose )
            System.out.println( "On recupere simplement les " + blockSize + " premiers bits");

        Iterator<Boolean> input = inputBitSequence.iterator();
        while( input.hasNext() ){
            for( int i=0 ; i<blockSize ; i++)
                buffer[i] = input.next();
            for ( int i=0 ; i<keyLineCount ; i++ )
                if( input.hasNext()) {
                    System.out.print("a");
                    input.next();
                }
            System.out.print("b");;
            output.append( buffer );
        }


        return output;
    }

    public boolean verify(BitSequence inputBitSequence, boolean verbose ){

        BitMatrix decodeMatrix = new BitMatrix( blockSize + keyLineCount, keyLineCount);

        for( int i=0 ; i<keyLineCount ; i++ ){
            for ( int j=0 ; j<blockSize ; j++ ){
                decodeMatrix.set( j, i, (int)(j/Math.pow( 2, (double)i ) )%2==0 );
            }

            decodeMatrix.set( i+blockSize , i, true );
        }
        if( verbose )
            System.out.println( decodeMatrix );


        BitMatrix inputMatrix = new BitMatrix( 1, blockSize+keyLineCount );

        Iterator<Boolean> input = inputBitSequence.iterator();

        while( input.hasNext() ){
            for( int i=0 ; i<blockSize+keyLineCount ; i++)
                inputMatrix.set( 0, i, input.next());
            if(!BitMatrix.multiply( decodeMatrix, inputMatrix).isNull())
                return false;
        }

        return true;
    }

    @Override
    public BitSequence correct(BitSequence inputBitSequence, boolean verbose) {
        return null;
    }
}

