package codecorrecteur;

import bitsequence.BitSequence;

public abstract class CodeCorrecteur {

    private static final boolean defaultVerboseValue = false;


    protected static String name;
    protected static boolean corrector;


    public BitSequence encoder(BitSequence inputBitSequence){
        return encoder( inputBitSequence, defaultVerboseValue );
    }

    public abstract BitSequence encoder(BitSequence inputBitSequence, boolean verbose );



    public BitSequence decoder(BitSequence inputBitSequence){
        return decoder( inputBitSequence, defaultVerboseValue );
    }

    public abstract BitSequence decoder(BitSequence inputBitSequence, boolean verbose );


    public boolean verify( BitSequence inputBitSequence){
        return verify( inputBitSequence, defaultVerboseValue );
    }

    public abstract boolean verify(BitSequence inputBitSequence, boolean verbose );


    public BitSequence correct(BitSequence inputBitSequence){
        return correct( inputBitSequence, defaultVerboseValue );
    }

    public abstract BitSequence correct(BitSequence inputBitSequence, boolean verbose );

    public String getName() {
        return name;
    }

    public boolean isCorrector() {
        return corrector;
    }
}
