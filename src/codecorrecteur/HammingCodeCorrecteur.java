package codecorrecteur;

import bitsequence.BitSequence;

import java.util.Arrays;
import java.util.Iterator;

public class HammingCodeCorrecteur extends CodeCorrecteur {

    private int blockSize;

    public HammingCodeCorrecteur(){
        this( 4 );
    }

    private HammingCodeCorrecteur(int blockSize){
        this.blockSize = blockSize;
        corrector = true;
        name = "Code de hamming";
    }


    @Override
    public BitSequence encoder(BitSequence inputBitSequence, boolean verbose) {

        boolean blockoutput[] = new boolean[7];
        Iterator<Boolean> input = inputBitSequence.iterator();
        BitSequence output = new BitSequence();

        while ( input.hasNext() ){
            blockoutput[2] =  input.hasNext()? input.next() : false;
            blockoutput[4] =  input.hasNext()? input.next() : false;
            blockoutput[5] =  input.hasNext()? input.next() : false;
            blockoutput[6] =  input.hasNext()? input.next() : false;
            blockoutput[0] =  blockoutput[2] ^ blockoutput[4] ^ blockoutput[6];
            blockoutput[1] =  blockoutput[2] ^ blockoutput[5] ^ blockoutput[6];
            blockoutput[3] =  blockoutput[4] ^ blockoutput[5] ^ blockoutput[6];
            output.append( blockoutput );
            if( verbose )
                System.out.println(Arrays.toString(blockoutput));
        }


        return output ;
    }

    @Override
    public BitSequence decoder(BitSequence inputBitSequence, boolean verbose) {
        boolean block[] = new boolean[4];
        Iterator<Boolean> input = inputBitSequence.iterator();
        BitSequence output = new BitSequence();

        while ( input.hasNext() ){
            input.next();
            input.next();
            block[0] = input.next();
            input.next();
            block[1] = input.next();
            block[2] = input.next();
            block[3] = input.next();
            output.append( block );
            if( verbose )
                System.out.println(Arrays.toString(block));
        }


        return output ;
    }

    @Override
    public boolean verify(BitSequence inputBitSequence, boolean verbose) {
        boolean[] block = new boolean[7];
        Iterator<Boolean> input = inputBitSequence.iterator();

        while ( input.hasNext() ){
            for(int i=0 ; i<block.length ; i++)
                block[i] = input.next();

            if( syndrom( block , verbose)!=0 )
                return false;

        }

        return true;
    }

    @Override
    public BitSequence correct(BitSequence inputBitSequence, boolean verbose) {
        boolean[] block = new boolean[7];
        BitSequence output = new BitSequence();
        Iterator<Boolean> input = inputBitSequence.iterator();

        int syndrom;

        while ( input.hasNext() ){
            for(int i=0 ; i<block.length ; i++)
                block[i] = input.next();

            syndrom = syndrom( block, verbose);

            if( syndrom( block , verbose)!=0 )
                block[syndrom-1]= ! block[syndrom-1];

            output.append(block);

        }

        return output;
    }

    private int syndrom(boolean[] block, boolean verbose){
        int syndrom = 0;

        if( block[0] ^ block[2] ^ block[4] ^ block[6])
            syndrom += 1;

        if( block[1] ^ block[2] ^ block[5] ^ block[6])
            syndrom += 2;

        if( block[3] ^ block[4] ^ block[5] ^ block[6])
            syndrom += 4;

        return syndrom;
    }

}
