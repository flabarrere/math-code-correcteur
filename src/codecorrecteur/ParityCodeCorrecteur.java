package codecorrecteur;

import bitsequence.BitSequence;

import java.util.Arrays;
import java.util.Iterator;

public class ParityCodeCorrecteur extends CodeCorrecteur {

    private int blockSize;

    public ParityCodeCorrecteur(){
        this( 7);
    }

    private ParityCodeCorrecteur(int blockSize){
        this.blockSize = blockSize;
        corrector = false;
        name = "Bit de paritée";
    }


    @Override
    public BitSequence encoder(BitSequence inputBitSequence, boolean verbose) {

        boolean[] block = new boolean[blockSize + 1];
        Iterator<Boolean> input = inputBitSequence.iterator();
        BitSequence output = new BitSequence();

        while ( input.hasNext() ){
            for( int i=0 ; i<blockSize ; i++) {
                block[i] =  input.hasNext()? input.next() : false;
                block[blockSize] = block[blockSize] ^ block[i];
            }
            output.append( block );
        }


        return output ;
    }

    @Override
    public BitSequence decoder(BitSequence inputBitSequence, boolean verbose) {
        boolean[] block = new boolean[blockSize];
        Iterator<Boolean> input = inputBitSequence.iterator();
        BitSequence output = new BitSequence();

        while ( input.hasNext() ){
            for( int i=0 ; i<blockSize ; i++) {
                block[i] = input.next();
            }
            output.append( block );
            input.next();
        }

        return output ;
    }

    @Override
    public boolean verify(BitSequence inputBitSequence, boolean verbose) {
        Iterator<Boolean> input = inputBitSequence.iterator();
        boolean parity = true;

        while ( input.hasNext() ){
            parity = parity ^ input.next();
        }

        return parity;
    }

    @Override
    public BitSequence correct(BitSequence inputBitSequence, boolean verbose) {
        return null;
    }
}
