package bitsequence;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class BitSequence implements Comparable<BitSequence>, Iterable<Boolean>{

    private boolean[] sequence;

    public BitSequence(){
        this( new boolean[0] );
    }

    public BitSequence(boolean[] sequence){
        this.sequence=sequence;
    }

    public BitSequence( String inputString){
        sequence = new boolean[inputString.length()];
        for( int i=0 ; i<inputString.length() ; i++ )
            sequence[i]=(inputString.charAt(i)=='1');
    }

    public void append(boolean[] addedSequence){

        boolean[] newSequence = new boolean[sequence.length+addedSequence.length];
        System.arraycopy(sequence, 0, newSequence, 0, sequence.length);
        System.arraycopy(addedSequence, 0, newSequence, sequence.length, addedSequence.length);
        sequence = newSequence;
    }

    public void append(BitSequence addedSequence){
        append( addedSequence.toArray() );
    }

    public int length() {
        return sequence.length;
    }

    public boolean[] toArray() {
        return sequence.clone();
    }

    public String toString(){
        StringBuilder bitWord = new StringBuilder();

        for( boolean b : sequence )
            bitWord.append( b? '1' : '0' );

        return bitWord.toString();
    }

    @Override
    public int compareTo(BitSequence o) {
        int differenceCount = 0;

        Iterator thisIterator = this.iterator();
        Iterator otherIterator = o.iterator();

        while( thisIterator.hasNext() || otherIterator.hasNext() ){

            if( !thisIterator.hasNext() ||
                    !otherIterator.hasNext()||
                    thisIterator.next()!=otherIterator.next())
                differenceCount ++;
        }

        return differenceCount;
    }

    @Override
    public boolean equals(Object obj) {
        if( !( obj instanceof BitSequence ))
            return false;
        return this.compareTo( (BitSequence) obj ) == 0 ;
    }

    @Override
    public Iterator<Boolean> iterator() {
        return new BitMatrixIterator() ;
    }

    private class BitMatrixIterator implements Iterator<Boolean>{

        int index = -1;
        @Override
        public boolean hasNext() {
            return index+1 < sequence.length ;
        }

        @Override
        public Boolean next() {

            if( !this.hasNext() )
                throw new NoSuchElementException();
            index++;
            return sequence[index];
        }
    }
}
