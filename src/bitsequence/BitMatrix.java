package bitsequence;

public class BitMatrix implements Cloneable{

    private int rowCount;
    private int columnCount;
    private boolean values[][];// values [y][x]

    public BitMatrix(int rowCount, int columnCount){

        if( rowCount <=0 || columnCount <=0 )
            throw new ArithmeticException("Les tailles de matrices doivent etre positive");
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        values = new boolean[columnCount][rowCount];
    }


    int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    boolean get(int row, int column){
        return values[column][row];
    }

    public void set(int row, int column, boolean value){
        values[column][row] = value;
    }

    public boolean[] getColumn(int column){
        boolean[] rowValues = new boolean[columnCount];
        for( int i=0 ; i<columnCount ; i++)
            rowValues[i]=values[i][column];
        return rowValues;
    }

    public void setColumn(int column, boolean[] columnValues){
        for( int i=0 ; i<columnCount ; i++)
            values[i][column] = columnValues[i];
    }

    boolean[] getRow(int row){
        boolean[] rowValues = new boolean[rowCount];
        for( int i=0 ; i<rowCount ; i++)
            rowValues[i]=values[i][row];
        return rowValues;
    }

    public void setRow( int row, boolean[] rowValues){
        for( int i=0 ; i<rowCount ; i++)
            values[i][row] = rowValues[i];
    }

    public boolean[][] toArray() {
        return values.clone();
    }

    public void add(BitMatrix otherMatrix)
    {
        for(int j = 0; j<otherMatrix.getColumnCount() ; j++ )
            for(int i = 0; i<otherMatrix.getRowCount() ; i++ )
                values[j][i] = values[j][i] ^ otherMatrix.get( i, j );
    }

    public boolean isNull(){
        for( boolean[] row : values)
            for( boolean value : row )
                if( value )
                    return false;
        return true;
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for( boolean[] row : values ){
            str.append('(');
            for( boolean  value : row )
            {
                str.append(' ');
                str.append(value ? '1' : '0');
            }
            str.append( " )\n" );
        }


        return str.toString();
    }

    public static BitMatrix getIdendityMatrix(int size){
        BitMatrix identityMatrix = new BitMatrix( size, size );
        for( int i=0 ; i<size ; i++ )
            identityMatrix.set( i, i, true );
        return identityMatrix;
    }


    public static BitMatrix add( BitMatrix matrix1, BitMatrix matrix2 )
    {
        BitMatrix sumMatrix = new BitMatrix( Math.max( matrix1.getRowCount(), matrix2.getRowCount()),
                Math.max( matrix1.getColumnCount(), matrix2.getColumnCount()));
        sumMatrix.add( matrix1 );
        sumMatrix.add( matrix2 );

        return sumMatrix;
    }

    public static BitMatrix multiply(BitMatrix matrix1, BitMatrix matrix2)
    {
        if(  matrix1.getRowCount() != matrix2.getColumnCount() )
            throw new ArithmeticException( "Les tailles de matrices ne sont pas propice a la multiplication" );

        BitMatrix productMatrix = new BitMatrix( matrix2.getRowCount(), matrix1.getColumnCount() );

        boolean value;

        for(int j = 0; j<productMatrix.getColumnCount() ; j++ )
        {
            for(int i = 0; i<productMatrix.getRowCount() ; i++ )
            {
                value = false;
                for(int k = 0; k<matrix1.getRowCount() ; k++ )
                {
                    if( matrix1.get( k, j) && matrix2.get( i, k))
                        value = !value;
                }
                productMatrix.set( i, j, value);

            }
        }
        return productMatrix;
    }

}
