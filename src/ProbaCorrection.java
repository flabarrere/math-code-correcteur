import bitsequence.BitSequence;;
import codecorrecteur.CLSCodeCorrecteur;
import codecorrecteur.CodeCorrecteur;
import codecorrecteur.HammingCodeCorrecteur;
import codecorrecteur.ParityCodeCorrecteur;
import scrambler.RandomScrambler;
import scrambler.Scrambler;

public class ProbaCorrection {

    public static void main( String arg[]){

        CodeCorrecteur codeCorrecteur = new ParityCodeCorrecteur();//remplacer cette ligne pour tester d'autre code corecteur
        Scrambler scrambler = new RandomScrambler( 0.01 );
        BitSequence bitSequenceOriginal = new BitSequence("10101010");
        BitSequence bitSequenceSent;
        BitSequence bitSequenceReceive;

        int cptFound = 0;
        int cptFail = 0;
        int cptCorrect = 0;
        for(int i=0 ; i<1000000 ; i++ ) {
            //on encode le message
            bitSequenceSent = codeCorrecteur.encoder(bitSequenceOriginal, false);

            //on envoie le message
            bitSequenceReceive = scrambler.scramble(bitSequenceSent);

            if( !bitSequenceReceive.equals(bitSequenceSent) )
                cptFail++;

            if( !codeCorrecteur.verify( bitSequenceReceive )) {
                cptFound++;

                if( codeCorrecteur.isCorrector()
                        && codeCorrecteur.correct( bitSequenceReceive).equals(bitSequenceSent))
                    cptCorrect++;
            }
        }
        System.out.println( cptFail + " Erreurs");
        System.out.println( cptFound + " Detectées " + cptFound/(double)cptFail);

        if( codeCorrecteur.isCorrector())
            System.out.println( cptCorrect + " Corrigées " + cptCorrect/(double)cptFound);
    }
}
